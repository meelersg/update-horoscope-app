var CreateArrayOfNumbers = function CreateArrayOfNumbers (start, end) {
        let myArray=[];
        
        for(let i = start; i<=end; i++){
            myArray.push(i);
        }

        return myArray;
    }                                                   

    var numbersArray = CreateArrayOfNumbers(0,21);

export default {CreateArrayOfNumbers, numbersArray};