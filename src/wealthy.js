import React, {useEffect} from 'react';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Button from '@mui/material/Button';
import { createTheme } from '@mui/material/styles';
import WealthOnClick from './WealthOnClick';
import ReSet from './ReSet';
import "./style.css";
import jobImage from "./job.svg";
import moneyImage from "./wealthy.svg";
import loveImage from "./love.svg";
import healthImage from "./health.svg";

function Copyright() {
  return (
    <Typography variant="body2" color="white" align="center" marginTop={"10px"}>
      {'Copyright © '}
      <Link color="inherit" href="./Login">
        Update Horoscope
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme();

export default function Predict() {
  useEffect(()=>{
    const token = localStorage.getItem('token')
    fetch('https://update-horoscope-app.herokuapp.com/authen', {
        method: 'POST', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+token
        }        
    })
    .then(response => response.json())
    .then(data => {
        if (data.status === 'ok') {
          alert('ท่านเลือกหัวข้อ "การเงิน" ขอให้ท่านร่ำรวยสมดังปรารถนา')        
        }else{
          alert('กรุณาลงชื่อเข้าระบบก่อนนะคะ')
          localStorage.removeItem('token');
          window.location = './login'
        }
    })
    .catch((error) => {
        console.error('Error:', error);
    });
  }, [])

  const handleLogout = (event) => {
    event.preventDefault();
    localStorage.removeItem('token');
    window.location = '/login'
  }

  return (
  <div>
    <meta charSet="UTF-8" />
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="style.css" />
    <title>Update Horoscope</title>
    <section className="navbar">
      <div className="logo">
        <h1>Update HORO</h1>
      </div>
      <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">ไพ่ยิปซี</a></li>
        <li><a href="#">โหราเลขศาสตร์</a></li>
        <li><a href="#">ติดต่อ-สอบถาม</a></li>
      </ul>
    </section>
    <p /><h2 id="view">พร้อมให้คำทำนายเรื่อง "การเงิน" อธิษฐานแล้วกดปุ่มเปิดคำทำนาย หรือเลือกหัวข้ออื่นที่ต้องการ</h2><p />
    <section className="choosingLists">
      <ul className="choosing">
        <li className="link1"><a href="./occupation" ><img className="topic" src={jobImage} alt="เรื่องการงาน" /><br /><h2>การงาน</h2></a> </li>
        <li className="link1"><a href="./wealthy" ><img className="topic" src={moneyImage} alt="เรื่องการเงิน" /><br /><h2>การเงิน</h2></a></li>
        <li className="link1"><a href="./love" ><img className="topic" src={loveImage} alt="เรื่องความรัก" /><br /><h2>ความรัก</h2></a></li>
        <li className="link1"><a href="./health" ><img className="topic" src={healthImage} alt="เรื่องสุขภาพ" /><br /><h2>สุขภาพ</h2></a></li>
      </ul>
    </section>
    <header>
      <div className="container">
        <div className="content">
          <p /><h1 id="output" style={{textAlign: 'center'}}>
            ตั้งจิตอธิษฐานแล้วคลิกเปิดคำทำนาย
          </h1><p />
          <button className="button button5" onClick={WealthOnClick}>เปิดคำทำนาย</button>
          <p><button className="button button4" onClick={ReSet}>สับใหม่</button></p>
          <h2 id="output2">ดูไพ่ที่ท่านได้รับ"</h2>
        </div>
      </div>
    </header>
    <section className="cards">
      <div>
        <h3 className="cardNo card1" id="cardNo1">Card No.</h3>
        <p className="cardNo"><img alt="รูปการ์ดใบที่ 1" id="pcardR1" src="https://img.freepik.com/premium-vector/hand-drawn-mystical-tarot-mobile-wallpaper_52683-80180.jpg?size=338&ext=jpg" /></p>
        <h2 className="cardNo card4" id="cardName1">Your Card No.1</h2>
      </div>
      <div>
        <h3 className="cardNo card2" id="cardNo2">Card No.</h3>
        <p className="cardNo"><img alt="รูปการ์ดใบที่ 2" id="pcardR2" src="https://img.freepik.com/premium-vector/hand-drawn-mystical-tarot-mobile-wallpaper_52683-80180.jpg?size=338&ext=jpg" /></p>
        <h2 className="cardNo card4" id="cardName2">Your Card No.2</h2>
      </div>
      <div>
        <h3 className="cardNo card3" id="cardNo3">Card No.</h3>
        <p className="cardNo"><img alt="รูปการ์ดใบที่ 3" id="pcardR3" src="https://img.freepik.com/premium-vector/hand-drawn-mystical-tarot-mobile-wallpaper_52683-80180.jpg?size=338&ext=jpg" /></p>
        <h2 className="cardNo card4" id="cardName3">Your Card No.3</h2>
      </div>
    </section>
    <section className="prediction">
      <div className="fortune">
        <div id="fortuneDetail">
          <p id="fDetail">อ่านคำทำนายได้จากตรงนี้</p>
          <p id="Detail1" />
          <p id="Detail2" />
          <p id="Detail3" />
          <p id="endDetail">หากท่านยังมีข้อสงสัยในเรื่องนี้ สามารถตั้งจิตให้มั่นนึกคำถามที่ยังอยากได้คำตอบ แล้วย้อนขึ้นไปคลิกเปิดคำทำนายเพิ่มเติมได้จนกว่าไพ่จะหมดกอง</p>
        </div>
      </div>
    </section>
    <footer>
      <Button variant="contained" onClick={handleLogout}>Logout</Button>
      <Copyright />
    </footer>
  </div>)
}