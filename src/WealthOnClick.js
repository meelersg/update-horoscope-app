import GetRandomNumber from './GetRandomNumber';
import nameCardArray from './NameCardArray';
import CreateArrayOfNumbers from './CreateArrayOfNumbers';
import Predict from "./Predict.json";


var OccOnClick = function OccOnClick () {

    var nameCardsArray = nameCardArray;
    var numbersArray = CreateArrayOfNumbers.numbersArray;
    var randomIndex = GetRandomNumber(0,numbersArray.length-1);
    var randomNumber = numbersArray[randomIndex];
    
    if(numbersArray.length < 3){
        alert("เปิดไพ่หมดกองแล้ว");
        document.getElementById("output").innerHTML = "คลิกปุ่ม สับไพ่ใหม่ อีกครั้ง";
        return;
    };

    var randomNumbers =[];
    var randomNameCards =[];

    randomNumbers.push(randomNumber);
    randomNameCards.push(nameCardsArray[randomIndex]);
    numbersArray.splice(randomIndex, 1);
    nameCardsArray.splice(randomIndex, 1);

    randomIndex = GetRandomNumber(0,numbersArray.length-1);
    randomNumber = numbersArray[randomIndex];
    randomNumbers.push(randomNumber);
    randomNameCards.push(nameCardsArray[randomIndex]);
    numbersArray.splice(randomIndex, 1);
    nameCardsArray.splice(randomIndex, 1);

    randomIndex = GetRandomNumber(0,numbersArray.length-1);
    randomNumber = numbersArray[randomIndex];
    randomNumbers.push(randomNumber);
    randomNameCards.push(nameCardsArray[randomIndex]);
    numbersArray.splice(randomIndex, 1);
    nameCardsArray.splice(randomIndex, 1);
    
    var output = document.getElementById("output");
    var output2 = document.getElementById("output2");
    var cardNo1 = document.getElementById("cardNo1");
    var cardN1 = document.getElementById("cardName1");
    var cardNo2 = document.getElementById("cardNo2");
    var cardN2 = document.getElementById("cardName2");
    var cardNo3 = document.getElementById("cardNo3");
    var cardN3 = document.getElementById("cardName3");

    document.getElementById("view").innerHTML =" ";
    output.innerHTML = "ไพ่ที่ท่านได้รับคือ.."
    output2.innerHTML = randomNameCards[0]+",  "+randomNameCards[1]+",  "+randomNameCards[2]
    cardNo1.innerHTML = "Card No. "+randomNumbers[0]
    cardN1.innerHTML = randomNameCards[0]
    cardNo2.innerHTML = "Card No. "+randomNumbers[1]
    cardN2.innerHTML = randomNameCards[1]
    cardNo3.innerHTML = "Card No. "+randomNumbers[2]
    cardN3.innerHTML = randomNameCards[2]

    var gOrbCard1 = GetRandomNumber(1,2);
    var gOrbCard2 = GetRandomNumber(1,2);
    var gOrbCard3 = GetRandomNumber(1,2);

    var PcardR1 = document.getElementById("pcardR1");
    PcardR1.style = "auto";

    var Detail1 = document.getElementById("Detail1");
    Detail1.innerHTML = " ";

    var Predicts = Predict;

    if (gOrbCard1 === 1) {
        PcardR1.src = `./pictures/${randomNumbers[0]}.png`;

        for (var i = 0; i < Predicts.length; i++) {
            if (Predicts[i].cName === randomNameCards[0]){
            Detail1.innerHTML=`${Predicts[i].cName}, ${Predicts[i].gMoney}<br/>`;
            }
        } 

    } else {
        PcardR1.src = `./pictures/${randomNumbers[0]}.png`;
        PcardR1.style.transform = "rotate(180deg)";

        for (var i = 0; i < Predicts.length; i++) {
            if (Predicts[i].cName === randomNameCards[0]){
            Detail1.innerHTML=`${Predicts[i].cName},${Predicts[i].bMoney}<br/>`;
            }
        }
        
    }

    var PcardR2 = document.getElementById("pcardR2");
    PcardR2.style="auto";

    var Detail2 = document.getElementById("Detail2");
    Detail2.innerHTML = " ";

    if (gOrbCard2 === 1) {
        PcardR2.src=`./pictures/${randomNumbers[1]}.png`;

        for (var i = 0; i < Predicts.length; i++) {
            if (Predicts[i].cName === randomNameCards[1]){
                if (gOrbCard1 === 1) {
                    Detail2.innerHTML=`และแทบจะไม่ต้องกังวล เพราะใบที่สองได้ ${Predicts[i].cName} ${Predicts[i].gMoney}`;
                    } else {
                    Detail2.innerHTML=`แม้ว่าคำทำนายจากใบแรกจะดูแย่ แต่ยังมีเรื่องดีจากใบที่สอง คือ ${Predicts[i].cName} ${Predicts[i].gMoney}`;
                    }
            }
        } 
    } else {
        PcardR2.src=`./pictures/${randomNumbers[1]}.png`;
        PcardR2.style.transform="rotate(180deg)";
        for (var i = 0; i < Predicts.length; i++) {
            if (Predicts[i].cName === randomNameCards[1]){
                if (gOrbCard1 === 1) {
                    Detail2.innerHTML=`แม้ว่าคำทำนายจากใบแรกจะดี แต่ควรระวังเพราะใบที่สองคือ ${Predicts[i].cName} กลับหัว ${Predicts[i].bMoney}`;
                } else {
                    Detail2.innerHTML=`แทบจะยืนยันได้ว่าต้องระมัดระวัง จากใบที่สอง คือ ${Predicts[i].cName} กลับหัว ${Predicts[i].bMoney}`;
                }
            }
        }
    }

    var PcardR3 = document.getElementById("pcardR3");
    PcardR3.style="auto";

    var Detail3 = document.getElementById("Detail3");
    Detail3.innerHTML = " ";

    if (gOrbCard3 === 1) {
        PcardR3.src=`./pictures/${randomNumbers[2]}.png`;
        for (var i = 0; i < Predicts.length; i++) {
            if (Predicts[i].cName === randomNameCards[2]){
                if (gOrbCard1 === 1 && gOrbCard2 === 1) {
                    Detail3.innerHTML=`สรุป จาก ${Predicts[i].cName} ยืนยันได้ว่า คุณจะมีข่าวดีๆ หรือหมดกังวลในเรื่องการงานของคุณอย่างแน่นอน ไพ่ใบนี้บอกว่า ${Predicts[i].gMoney}`;
                } if (gOrbCard1 === 1 && gOrbCard2 === 2){
                    Detail3.innerHTML=`สรุป สุดท้ายคุณจะสมหวัง จากไพ่ ${Predicts[i].cName} ${Predicts[i].gMoney}`;
                } if (gOrbCard1 === 2 && gOrbCard2 === 1){
                    Detail3.innerHTML=`สรุป สุดท้ายคุณจะสมหวัง จากไพ่ ${Predicts[i].cName} ${Predicts[i].gMoney}`;
                } if (gOrbCard1 === 2 && gOrbCard2 === 2){
                    Detail3.innerHTML=`สรุป สุดท้ายคุณยังมีโอกาสสมหวัง จากไพ่ ${Predicts[i].cName} ${Predicts[i].gMoney}`;
                }
            }
        }
    } else {
        PcardR3.src=`./pictures/${randomNumbers[2]}.png`;
        PcardR3.style.transform="rotate(180deg)";
        for (var i = 0; i < Predicts.length; i++) {
            if (Predicts[i].cName === randomNameCards[2]){
                if (gOrbCard1 === 1 && gOrbCard2 === 1) {
                    Detail3.innerHTML=`สรุป แม้ว่าจะได้รับคำทำนายที่ดีจากไพ่สองใบแรก แต่ใบสรุป ${Predicts[i].cName} ที่กลับหัว ไม่ยืนยันว่าจะสมหวังทั้งหมด เพราะ ${Predicts[i].bMoney}`;
                } if (gOrbCard1 === 1 && gOrbCard2 === 2){
                    Detail3.innerHTML=`สรุป จากไพ่ ${Predicts[i].cName} กลับหัว คุณควรระมัดระวัง ${Predicts[i].bMoney}`;
                } if (gOrbCard1 === 2 && gOrbCard2 === 1){
                    Detail3.innerHTML=`สรุป จากไพ่ ${Predicts[i].cName} กลับหัว คุณควรระมัดระวัง ${Predicts[i].bMoney}`;
                } if (gOrbCard1 === 2 && gOrbCard2 === 2){
                    Detail3.innerHTML=`สรุป จากไพ่ ${Predicts[i].cName} กลับหัว ทุกเรื่องที่คุณกังวลอาจเกิดขึ้นได้ สิ่งที่จะเปลี่ยนชะตาร้ายให้กลายเป็นดีคือการสร้างบุญใหญ่ในปัจจุบัน ไพ่สรุปมีข้อกังวลคือ ${Predicts[i].bMoney}`;
                }
            }
        }
    }

    return;
}
export default OccOnClick;