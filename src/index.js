import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import reportWebVitals from './reportWebVitals'
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Login from './Login'
import Predict from './Predict'
import Register from './Register'
import Occupation from './occupation'
import Love from './love'
import Health from './health'
import Wealthy  from './wealthy'

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/login" element={<Login />} />
      <Route path="/predict" element={<Predict />} />
      <Route path="/register" element={<Register />} />
      <Route path="/occupation" element={<Occupation />} />
      <Route path="/love" element={<Love />} />
      <Route path="/health" element={<Health />} />
      <Route path="/wealthy" element={<Wealthy />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
